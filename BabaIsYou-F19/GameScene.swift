//
//  GameScene.swift
//  BabaIsYou-F19
//
//  Created by Parrot on 2019-10-17.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {

    var BABA_SPEED : CGFloat! = 30
    var baba : SKSpriteNode!
    var flag : SKSpriteNode!
    var flagBlock : SKSpriteNode!
    var isBlock : SKSpriteNode!
    var winBlock : SKSpriteNode!
    var stopBlock : SKSpriteNode!
    var wallBlock : SKSpriteNode!
    var babaXPosition : CGFloat!
    var babaYPosition : CGFloat!
    
    override func didMove(to view: SKView) {
        self.physicsWorld.contactDelegate = self
        
        //baba
        self.baba = self.childNode(withName: "baba") as! SKSpriteNode
        babaXPosition = baba.position.x
        babaYPosition = baba.position.y
        self.baba.physicsBody = SKPhysicsBody(rectangleOf: baba.size)
        self.baba.physicsBody?.affectedByGravity = false
        self.baba.physicsBody?.categoryBitMask = 1
        
        //Flag block
        self.flagBlock = self.childNode(withName: "flagblock") as! SKSpriteNode
        self.flagBlock.physicsBody = SKPhysicsBody(rectangleOf: flagBlock.size)
        self.flagBlock.physicsBody?.affectedByGravity = false
        self.flagBlock.physicsBody?.categoryBitMask = 2
        
        //wall block
        self.wallBlock = self.childNode(withName: "wallblock") as! SKSpriteNode
        self.wallBlock.physicsBody = SKPhysicsBody(rectangleOf: wallBlock.size)
        self.wallBlock.physicsBody?.affectedByGravity = false
        self.wallBlock.physicsBody?.categoryBitMask = 4
        
        //Stop Block
        self.stopBlock = self.childNode(withName: "stopblock") as! SKSpriteNode
        self.stopBlock.physicsBody = SKPhysicsBody(rectangleOf: stopBlock.size)
        self.stopBlock.physicsBody?.affectedByGravity = false
        self.stopBlock.physicsBody?.categoryBitMask = 8
        
        //is block
        self.isBlock = self.childNode(withName: "isblock") as! SKSpriteNode
        //win block
        self.winBlock = self.childNode(withName: "winblock") as! SKSpriteNode
        self.winBlock.physicsBody = SKPhysicsBody(rectangleOf: winBlock.size)
        self.winBlock.physicsBody?.affectedByGravity = false
        self.winBlock.physicsBody?.categoryBitMask = 16
        
        //flag
        self.flag = self.childNode(withName: "flag") as! SKSpriteNode
//        self.flag.physicsBody = SKPhysicsBody(rectangleOf: flag.size)
//        self.flag.physicsBody?.affectedByGravity = false
//        self.flag.physicsBody?.categoryBitMask = 32
        self.flag.physicsBody?.contactTestBitMask = 1
        
        
    
    }
   
    func didBegin(_ contact: SKPhysicsContact) {
        print("Something collided!")
        let nodeA = contact.bodyA.node
        let nodeB = contact.bodyB.node
        
        if (nodeA == nil || nodeB == nil) {
            return
        }
        //for flag is win: Player wins when baba collides with flag
        if((nodeA!.name == "flag" && nodeB!.name == "baba") || (nodeA!.name == "baba" && nodeB!.name == "flag")) {
            print("BABA WON")
        }
    }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    
        let mouseTouch = touches.first
        if (mouseTouch == nil) {
            return
        }
        let location = mouseTouch!.location(in: self)
        let nodeTouched = atPoint(location).name
        print("Baba Touched: \(nodeTouched)")
        
        if(nodeTouched == "MoveUp" ){
            self.baba.position.y = self.baba.position.y + BABA_SPEED
        }
        if nodeTouched == "MoveDown"{
            self.baba.position.y = self.baba.position.y - BABA_SPEED
        }
        if nodeTouched == "MoveLeft"{
            self.baba.position.x = self.baba.position.x - BABA_SPEED
        }
        if nodeTouched == "MoveRight"{
            self.baba.position.x = self.baba.position.x + BABA_SPEED
        }
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {

    }
}
